#include "mask.h"
#include <stdio.h>

static int mask_owner, mask_group, mask_other;

int main() {
	mask_owner = 0;
	mask_group = 0;
	mask_other = 0;

	int *mask_owner_ptr = &mask_owner;
	int *mask_group_ptr = &mask_group;
	int *mask_other_ptr = &mask_other;

	char charOwner1, charOwner2, charOwner3, charGroup1, charGroup2, charGroup3, charOther1, charOther2, charOther3;


	printf("Permiso lectura para owner? SET [s/n] \n");
	scanf("%c", &charOwner1);
	if (charOwner1 == 's')
	{
		setPermiso(mask_owner_ptr, 'r', SET);
	}

	else {
		setPermiso(mask_owner_ptr, 'r', UNSET);
	}
	getchar();

	printf( "Permiso escritura para owner? [s/n] \n");
	scanf("%c", &charOwner2);		
	if (charOwner2 == 's')
	{
		setPermiso(mask_owner_ptr, 'w', SET);
	}

	else {
		setPermiso(mask_owner_ptr, 'w', UNSET);
	}
	getchar();

	printf( "Permiso ejecución para owner? [s/n] \n");
	scanf("%c", &charOwner3);
	if (charOwner3 == 's')
	{
		setPermiso(mask_owner_ptr, 'x', SET);
	}

	else {
		setPermiso(mask_owner_ptr, 'x', UNSET);
	}
	getchar();

	printf("Permiso lectura para group? [s/n] \n");
	scanf("%c", &charGroup1);
	if (charGroup1 == 's')
	{
		setPermiso(mask_group_ptr, 'r', SET);
	}

	else {
		setPermiso(mask_group_ptr, 'r', UNSET);
	}
	getchar();
	printf("Permiso escritura para group? [s/n] \n");
	scanf("%c", &charGroup2);
	if (charGroup2 == 's')
	{
		setPermiso(mask_group_ptr, 'w', SET);
	}

	else {
		setPermiso(mask_group_ptr, 'w', UNSET);
	}
	getchar();

	printf("Permiso ejecución para group? [s/n] \n");
	scanf("%c", &charGroup3);
	if (charGroup3 == 's')
	{
		setPermiso(mask_group_ptr, 'x', SET);
	}

	else {
		setPermiso(mask_group_ptr, 'x', UNSET);
	}
	getchar();

	printf("Permiso lectura para other? [s/n] \n");
	scanf("%c", &charOther1);
	if (charOther1 == 's')
	{
		setPermiso(mask_other_ptr, 'r', SET);
	}

	else {
		setPermiso(mask_other_ptr, 'r', UNSET);
	}
	getchar();

	printf("Permiso escritura para other? [s/n] \n");
	scanf("%c", &charOther2);
	if (charOther2 == 's')
	{
		setPermiso(mask_other_ptr, 'w', SET);
	}

	else {
		setPermiso(mask_other_ptr, 'w', UNSET);
	}
  	getchar();

	printf("Permiso ejecución para other? [s/n] \n");
	scanf("%c", &charOther3);
	if (charOther3 == 's')
	  {
	  	setPermiso(mask_other_ptr, 'x', SET);
	  }

	else {
		setPermiso(mask_other_ptr, 'x', UNSET);
	}
	getchar();

	printf("Máscara de permiso: %d%d%d \n", mask_owner, mask_group, mask_other);

}
