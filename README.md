# Práctica 6: Uso de máscaras y punteros #

Este es un repositorio esqueleto para la práctica 6 de la materia Programación de Sistemas (CCPG1008) P1 de la ESPOL.

### ¿Cómo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en cuenta (no este) en su computadora del laboratorio
* Completar la práctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

* Maria Belen Guaranda
* Galo Castillo