CC=gcc
CFLAGS=-I.
DEPS = mask.h
OBJ = main.o mask.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)
makeprog: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)
clean:
	rm -f $(OBJ)/*.o makeprog *.gch
